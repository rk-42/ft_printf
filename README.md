# ft_printf
  Tired of doing your postings by alternating ft_putstr and ft_putnbr?
  Aren't you allowed to use printf? Recode yours! This will be an opportunity to discover a feature of C - variadic functions - and to practice fine management of display options. You will then have the right to use your printf in all your subsequent projects.
  
  
Regardless of the programming language considered, the printf function,(or its equiv-alents) is always highly useful.
The main reason is the ease of its formatting, and thesupport of diverse types in variable numbers.
Some variations even propose to be able to write the resulting string of characters either in a file descriptor or in a particular stream.
Some also propose to recall this string without printing it. In short, undeniably, printf is a vital function.
In this project, we ask you to recode it and add it to yourlibft so that you can use it in all your future projects, such as ft_ls..

The versatility of the printf function in C represents a great exercise in programming for us.
This project is of moderate difficulty. It will enable you to discover variadic functions in C in a particularly relevant context as well as learn about a great example of a basic “dispatcher” in C via the use of an array of function’s pointers.
